// import { creaProdotto } from "./cartItem";
//dati utente
const you = [
  {
    nome: "Bobby",
    cognome: "Dog",
    isReturning: true,
  },
];

//dati prodotto
const prodotti = [
  {
    id: 0,
    immagine: "./img/iTem1.png",
    titoloProdotto:
      "TRAMEZZINI Prosciutto di Praga e mozzarella di Bufala + maionese (x1), Prosciutto crudo e mozzarella di Bufala + maionese (x1) MARITOZZI SALATI Baccalà mantecato e lamponi (x1), Mortadella, ricotta e granella di pistacchi (x1)",
    costoDiConsegna: 6.9,
    quantità: 1,
    prezzo: 39,
    spedizioneData: "Dalle 08:30 del 21-10-2022",
  },
  {
    id: 1,
    immagine: "./img/iTem2.jpg",
    titoloProdotto: "Cake al Limone da 6 a 12 porzioni 6/8 porzioni",
    costoDiConsegna: 6.9,
    quantità: 1,
    prezzo: 22,
    spedizioneData: "Dalle 08:30 del 21-10-2022",
  },
  {
    id: 2,
    immagine: "./img/item3.jpg",
    titoloProdotto:
      "RINVENIMENTO La Vaporiera (x1) SFIZI Edamame - classico/piccante - Classico (x1), Edamame - classico/piccante - Piccante (x1), Pickles - Cetriolini Sottaceto (x1)",
    costoDiConsegna: 6.9,
    quantità: 1,
    prezzo: 19,
    spedizioneData: "Dalle 18:00 di oggi",
  },
  {
    id: 3,
    immagine: "./img/item4.jpg",
    titoloProdotto: "Secchiello in zinco delicato  ",
    costoDiConsegna: 6.9,
    quantità: 1,
    prezzo: 50,
    spedizioneData: "Dalle 15:00 di oggi",
  },
];

populateUser(you);
totalValue(prodotti);
creaProdotto(prodotti);
