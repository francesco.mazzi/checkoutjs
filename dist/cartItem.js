// import you from "./index.js";
const titleDisplay = document.querySelector("#title");
const tabellaDisplay = document.querySelector("#tabellaCarta");
const totalProductDisplay = document.querySelector("#totalProduct");
const totalPriceDisplay = document.querySelector("#totalPrice");
//link button --> buttonCartLink
const buttonCheckout = document.getElementById("buttonCartLink");

//prova visione async
if (document.readyState == "loading") {
  document.addEventListener("DOMContentLoaded", ready);
} else {
  ready();
}

//lista prodotti
let prodottiList = [];
//click btn
let addClick = 0;

//Welcome
function populateUser() {
  const accesso = you[0].isReturning ? `Bentornato ${you[0].nome}, ` : "";
  titleDisplay.innerHTML = `<h2>${accesso} il tuo carrello contiene: </h2>`;
}

//funzione carrello
function ready() {
  //Delete button
  const deleteButton = document.querySelectorAll(".deleteBtn");
  const prodottoItem = document.querySelectorAll(".prodottoItem");
  deleteButton.forEach((item, i) => {
    item.addEventListener("click", (e) => {
      prodottiList = prodottiList.filter((data, index) => index != i);
      console.log(prodottiList);
      const buttonClickedRemove = e.target;
      prodottoItem[i].remove();
    });
  });

  //add button
  const addButton = document.querySelectorAll("#addButton");

  addButton.forEach((item, i) => {
    item.addEventListener("click", (e) => {
      prodottiList[i].quantità++;
      console.log(prodottiList);
      console.log("+ " + prodottiList[i].quantità);
    });
  });
  //minus button
  const minusButton = document.querySelectorAll("#removeButton");

  minusButton.forEach((item, i) => {
    item.addEventListener("click", (e) => {
      if (prodottiList[i].quantità > 0) {
        prodottiList[i].quantità--;
        console.log("- " + prodottiList[i].quantità);
      } else {
        addClick = 0;
        //delete di può fare unica funzione
        minusButton.forEach((item, i) => {
          item.addEventListener("click", (e) => {
            prodottiList = prodottiList.filter((data, index) => index != i);
            const buttonClickedRemove = e.target;
            prodottoItem[i].remove();
          });
        });
      }
    });
  });
}

//Totale cart value
function totalValue() {
  let somma = 0;
  for (let i = 0; i < prodotti.length; i++) {
    somma = prodotti[i].prezzo + somma;
  }
  const linkButtonText =
    prodotti.length > 0 ? `Concludi l'ordine` : "Continua gli acquisti";
  buttonCheckout.textContent = linkButtonText;
  const nProdotti =
    prodotti.length > 0 ? `Prodotti aggiunti: ${prodotti.length}` : "Non hai ";
  totalProductDisplay.innerHTML = nProdotti;
  const pProdotti = prodotti.length > 0 ? `Il totale è di ${somma} €` : "";
  totalPriceDisplay.innerHTML = pProdotti;
}
// product item
const creaProdotto = () => {
  for (let i = 0; i < prodotti.length; i++) {
    prodottiList.push(prodotti[i]);
  }
  for (let i = 0; i < prodottiList.length; i++) {
    // prodottiList.push(prodotti[i]);
    const prodottoItem = document.createElement("div");
    tabellaDisplay.appendChild(prodottoItem);
    prodottoItem.classList.add("prodottoItem");
    prodottoItem.innerHTML = `
    <button class="deleteBtn" id="${prodottiList[i].id} ">❌</button>
          <div id="img">
            <img src="${prodottiList[i].immagine}" alt="" />
          </div>
          <div>
            <span>Prodotto:</span>
            <div id="descrizioneProdottoItem">
              ${prodottiList[i].titoloProdotto}
            </div>
          </div>
          <div>
            <span>Costo di consegna:</span>
            <div id="prezzoSpedizione">${prodottiList[i].costoDiConsegna}</div>
          </div>
          <div>
            <span class="spedizione">Spedizione</span>
            <div id="dataSpedizione">${prodottiList[i].spedizioneData}</div>
          </div>
          <form class="addMinus">
            <button id="addButton" class="btn">+</button
            >${prodottiList[i].quantità}<button class="btn" id="removeButton">
              -
            </button>
          </form>
          <div>
            <span>Totale</span>
            <div id="prezzoTotale">${
              prodottiList[i].prezzo * prodottiList[i].quantità
            } €</div>
          </div>
    `;
  }
};
